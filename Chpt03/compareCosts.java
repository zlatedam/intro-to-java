import java.util.Scanner;
  
public class compareCosts {
  public static void main(String [] args) {
    Scanner input = new Scanner(System.in);

    // ask user to enter weight & price for package 1
    System.out.println("Enter weight and price for package 1: ");
    double weightOne = input.nextDouble();
    double priceOne = input.nextDouble();

    // same for package 2
    System.out.println("Enter weight and price for package 2: ");
    double weightTwo = input.nextDouble();
    double priceTwo = input.nextDouble();

    if (priceOne > priceTwo) {
      System.out.println("Package two has a better price");
    }
    if (priceOne < priceTwo) {
      System.out.println("Package one has a better price");
    }
    if (priceOne == priceTwo) { 
      System.out.println("Both packages have the same price");
    }
  }
}

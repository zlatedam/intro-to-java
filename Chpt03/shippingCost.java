import java.util.Scanner;

public class shippingCost {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Prompt user to enter the weight of the package
  System.out.println("Please enter the weight of your package (lbs)");
  double packageWeight = input.nextDouble();

  // Calculate the cost of shipping based on the package weight
  if (packageWeight > 0 && packageWeight <= 2) {
    System.out.println("Your package will cost $2.50 to ship");
  } 
  if (packageWeight > 2 && packageWeight <= 4) {
    System.out.println("Your package will cost $4.50 to ship");
  }
  if (packageWeight > 4 && packageWeight <= 10) {
    System.out.println("Your package will cost $7.50 to ship");
  }
  if (packageWeight > 10 && packageWeight <= 20) {
    System.out.println("Your package will cost $10.50 to ship");
  }
  if (packageWeight > 20) {
    System.out.println("Your package cannot be shipped");
  }
 }
}

import java.util.Scanner;
import java.util.Random;

public class MultiplyThreeNumbers {
  public static void main(String [] args) {
    Scanner input = new Scanner(System.in); 
    
  // Generate three random numbers between 1 - 10 
  int min = 1;
  int max = 10;
  int number1 = (int)Math.floor(Math.random()*(max-min+1)+min);
  int number2 = (int)Math.floor(Math.random()*(max-min+1)+min);
  int number3 = (int)Math.floor(Math.random()*(max-min+1)+min);

  // Prompt user to enter answer 
  System.out.println("What is the sum of " + number1 + "*" + number2 + "*" + number3 + "?");
  
  // Compute the answer 
  double answer = number1 * number2 * number3;
  double userAnswer = input.nextDouble();

  // See if user got it correct, if not ask them to try again
  if (userAnswer == answer)
    System.out.println("You are correct");
  else if (userAnswer != answer)
    System.out.println("Incorrect, please try again");

    }
}

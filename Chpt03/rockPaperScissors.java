import java.util.Scanner;
import java.util.Random;

public class rockPaperScissors {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);
  
  // Generate a random number between 0, 1, 2 for computer
  int computer = (int) Math.random() * 3;

  // ask user to input 0 for scissor, 1 for rock and 2 for paper
  System.out.println("Enter your pick - Scissor: 0, Rock: 1, Paper: 2");
  int answer = input.nextInt();

  // Compute if user won
  if (computer == 0 && answer == 0) {
      System.out.println("Tie");
  } if (computer == 0 && answer == 1) {
      System.out.println("You win");
  } if (computer == 0 && answer == 2) {
      System.out.println("You lose"); 
  } if (computer == 1 && answer == 0) {
      System.out.println("You lose");
  } if (computer == 1 && answer == 1) {
      System.out.println("Tie");
  } if (computer == 1 && answer == 2) {
      System.out.println("You win");
  } if (computer == 2 && answer == 0) {
      System.out.println("You win");
  } if (computer == 2 && answer == 1) {
      System.out.println("You lose");
  } if (computer == 2 && answer == 2) {
      System.out.println("Tie");
    }
 }
}

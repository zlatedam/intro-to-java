import java.util.Scanner;
import java.util.Random;

public class CoinFlip {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);
  
  System.out.println("Guess 0 for head, 1 for tails:");
  int answer = input.nextInt();
  int result = (int) (Math.random() * 2);

  if (answer == result) { 
    System.out.println("You are correct");
  } else {  
    System.out.println("Try again");
    }
  }
}

import java.util.Scanner;

public class Lottery {
  public static void main(String [] args) {
  // Generate a lottery number
  int lottery = (int) (Math.random() * 100);

  // Prompt the user to enter a guess 
  Scanner input = new Scanner(System.in);
  System.out.println("Enter your lottery pick (two digits): ");
  int guess = input.nextInt();
  
  // Get digits from the lottery
  int lotteryDigit1 = lottery / 10;
  int lotteryDigit2 = lottery % 10;

  // Get digits from guess
  int guessDigit1 = guess / 10;
  int guessDigit2 = guess % 10;

  System.out.println("The lottery number is " + lottery);

  // Check the guess
  if (guess == lottery)
    System.out.println("Exact match: you win $10,000"); // if they guess exactly (both digits)
  else if (guessDigit2 == lotteryDigit1  
         && guessDigit1 == lotteryDigit2) // Guess both digits but not in order
    System.out.println("Match all digits: you win $3,000");
  else if (guessDigit1 == lotteryDigit1 // Correct first digit  
        || guessDigit1 == lotteryDigit2 // or guess 1 = digit 2
        || guessDigit2 == lotteryDigit1 // or guess 2 = digit 1
        || guessDigit2 == lotteryDigit2) // or guess 2 = digit 2
    System.out.println("Match one digit: you win $1,000");
  else 
    System.out.println("Sorry, No match"); 
    }
}

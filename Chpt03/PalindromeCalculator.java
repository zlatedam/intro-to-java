import java.util.Scanner;

public class PalindromeCalculator {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Ask user to enter three numbers 
  System.out.println("Enter a three digit integer:");
  int number = input.nextInt();

  // Exract the digits from the user input 
  int first = number / 100;
  int last = number % 10;
  
  // Calculate
  if (last == first) {
    System.out.print(number + " is a palindrome");
  } else {  
    System.out.print(number + " is not a palindrome");
    }
  }
}

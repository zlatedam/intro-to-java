public class Exercise_3_13_3 {
  public static void main(String [] args) {

  int x = 1, a = 3;
  
  System.out.println("x prior to if statement = " + x);

  if (a == 1)
    x += 5;
  else if (a == 2)
    x += 10;
  else if (a == 3)
    x += 16;
  else if (a == 4)
    x += 34;

  System.out.println("x after if statement = " + x);

   }
}

import java.util.Scanner;

public class Checkpoint_3_14_3 {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  System.out.println("Please enter a number");
  double x = input.nextDouble();

  double scale = 1;
  double score;

  // Compute
  if (x > 10) 
    score = 3 * scale;
  else    
    score = 4 * scale;

  // Print results 
  System.out.println("score = " + score);

  }
}


import java.util.Scanner;
import java.lang.Math;

public class geometryCircle {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Prompt user to enter two points for the circle
  System.out.println("Enter a point with two coordinates:");
  int x = input.nextInt();
  int y = input.nextInt();

  // Compute if these points are within the circle or not 
  
  boolean withinCircle = 
          (Math.pow(Math.pow(x, 2) + Math.pow(y,2), 0.5) <= 10);

  // display results
  System.out.println("Point (" + x + ", "+ y + ") is " + ((withinCircle) ? "in " : "not in ") + "the circle");
  }
}


import java.util.Scanner;

public class HealthApplication {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Ask user for inputs 
  System.out.println("Enter weight in pounds");
  double weight = input.nextDouble();

  System.out.println("Enter Feet");
  double heightFeet = input.nextDouble();
  
  System.out.println("Enter inches");
  double heightInches = input.nextDouble();

  // double variables 
  final double KILOGRAMS_PER_POUND = 0.45359237;
  final double METERS_PER_INCH = 0.0254;
  
  heightInches = heightInches + heightFeet * 12;
  
  // Compute BMI
  double WeightInKilograms = weight * KILOGRAMS_PER_POUND;
  double HeightInMeters = heightInches * METERS_PER_INCH;
  double bmi = WeightInKilograms / (HeightInMeters * HeightInMeters);

  // print results 
  System.out.println("Your BMI is: " + bmi);
  if (bmi < 18.5)
    System.out.println("You are underweight");
  else if (bmi < 25)
    System.out.println("You are a normal weight");
  else if (bmi < 30)
    System.out.println("You are overweight");
  else 
    System.out.println("You are obese");

    }
  }


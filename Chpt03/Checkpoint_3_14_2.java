import java.util.Scanner;

public class Checkpoint_3_14_2 {
  public static void main(String [] args) {
  
  // Ask user to input their age
  Scanner input = new Scanner(System.in);
  System.out.println("Please enter your age");
  double ages = input.nextDouble();
  
  // Calculate ticket price based on their age input 
  double result = (ages >= 16) ? 20 : 10;

  // Print the ticket price 
  System.out.println("The price of the ticket is " + result);

  }

}

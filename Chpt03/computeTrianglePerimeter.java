import java.util.Scanner;

public class computeTrianglePerimeter {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Prompt user to enter three edges for a triangle
  System.out.println("Please enter three edges of a triangle");
  int a = input.nextInt();
  int b = input.nextInt();
  int c = input.nextInt();
  
  int answer = a + b + c;
  // Compute the perimeter 
  if (a + b > c ) {
    System.out.println("The perimeter of the triangle is " +  answer);


  }
 }
}

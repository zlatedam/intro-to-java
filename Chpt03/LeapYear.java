import java.util.Scanner;

public class LeapYear {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Ask user to enter a year 
  System.out.println("Please enter a year");
  int year = input.nextInt();

  // Check if the input is a leap year
  boolean isLeapYear = 
    (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);

  // Display the result
  System.out.println("Is " + year + " a leap year? " + isLeapYear);

  }
}

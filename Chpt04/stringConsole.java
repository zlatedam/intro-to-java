import java.util.Scanner;

public class stringConsole {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);

        // Prompt user to enter three string
        System.out.println("Enter three words separated by spaces");

        String s1 = input.next();
        String s2 = input.next();
        String s3 = input.next();
        
        // Print the results
        System.out.println("String s1 is " + s1);
        System.out.println("String s2 is " + s2);
        System.out.println("String s3 is " + s3);
    }
}

import java.util.Scanner;

public class socialSecurity {
    public static void main(String [] args) {
	Scanner input = new Scanner(System.in);

	// Ask user to input an SSN
	System.out.println("Enter an SSN: ");
	String ssn = input.nextLine();

	// Check whether the SSN input is valid
	boolean isValid = 
			(ssn.length() == 11) && 
			(Character.isDigit(ssn.charAt(0))) &&
			(Character.isDigit(ssn.charAt(1))) &&
			(Character.isDigit(ssn.charAt(2))) &&
			(ssn.charAt(3) == '-') &&
			(Character.isDigit(ssn.charAt(4))) &&
			(Character.isDigit(ssn.charAt(5))) &&
			(Character.isDigit(ssn.charAt(7))) &&
			(ssn.charAt(6) == '-') &&
			(Character.isDigit(ssn.charAt(8))) &&
			(Character.isDigit(ssn.charAt(9))) &&
			(Character.isDigit(ssn.charAt(10)));

	// Display the results
	System.out.println(ssn + " is " + ((isValid) ? " a valid " : "an invalid ")
			   + " social security number");
    }
}

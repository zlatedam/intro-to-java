public class Checkpoint_4_4_5 {
    public static void main(String [] args) {

    String s1 = " Welcome ";
    String s2 = " welcome ";

    // (A) if s1 = s2, assign boolean variable isEqual
    boolean isEqual = s1.equals(s2);

    // (B) if s1 is equal to s2 (ignoring case) result to isEqual
    boolean isEqual = s1.equalsIgnoreCase(s2);

    // (C) compare s1 to s2 and assign to int variable x
    int x = s1.compareTo(s2);

    // (D) compare s1 to s2 ignoring case and assign to variable int x
    int x = s1.equalsIgnoreCase(s2);

    // (E) check whether s1 has the prexif aaa and asign to boolean value b
    boolean b = s1.startsWith("AAA");

    // (F) check whether s1 ends with AAA and assign to boolean value b
    boolean b = s1.endsWith("AAA");

    // (G) assign the length of s1 to int variable x
    int x = s1.length();

    // (H) assign first character of s1 to char variable x
    char x = s1.charAt(0);

    // (I) create a new string that combines s1 with s2
    String s3 = s1.concat(s2);

    String s3 = s1 + s2;

    // (J) create a substring of s1 starting from index 1
    String s3 = s1.substring(1);

    // (K) create substring from 1 to 4
    String s3 = s1.substring(1, 5);

    // (L) create new string that convers s1 to lowercase
    String s3 = s1.toLowerCase();

    // (M) create new string that convers s1 to uppercase
    String s3 = s1.toUpperCase();

    // (N) create new string s3 that trims whitespaces of s1 both ends
    String s3 = s1.trim();

    // (O) assign the index of first character e in s1 to int variable x
    int x = s1.indexOf('e');

    // (P) assign the index of last occurance of string abc in s1 to variable x
    int x = s1.lastIndexOf("abc");

    }
}

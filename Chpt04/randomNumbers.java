import java.lang.Math;

public class randomNumbers {
  public static void main(String [] args) {

  // generate random number between 34 - 55
  double lower1 = 34;
  double upper1 = 55;
  
  double random1 = (double)(Math.random() * (upper1 - lower1 )) + lower1;
  System.out.println("Random number between 34 - 55 is " + random1);

  // generate random number between 0 - 999
  double lower2 = 0;
  double upper2 = 999;

  double random2 = (double)(Math.random() * (upper2 - lower2)) + lower2;
  System.out.println("Random number between 0 - 999 is " + random2);

  // generate random number between 5.5 55.5
  double lower3 = 5.5;
  double upper3 = 55.5;
  
  double random3 = (double)(Math.random() * (upper3 - lower3)) + lower3;
  System.out.println("Random number between 5.5 - 55.5 is " + random3);

  }
}

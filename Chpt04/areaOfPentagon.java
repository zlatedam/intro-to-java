import java.util.Scanner;
import java.lang.Math;

public class areaOfPentagon {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);

        // Ask user to input length from center to vertex
        System.out.println("Enter the length from the center to a vertex");
        double userLength = input.nextDouble();

        // Compute the area first
        double side = (2 * userLength) * Math.sin(Math.PI / 5);

        // Compute the area of a pentagon
        double area = (5 * Math.pow(side, 2) / (4 * Math.tan(Math.PI / 5)));

        // Print out the results
        System.out.printf("The area of the pentagon is %5.2f\n", area);

    }
}

import java.util.Scanner;
import java.lang.Math;

public class radiansToDegree {
  public static void main(String [] args) {
  Scanner input = new Scanner(System.in);

  // Ask user to enter degrees (in this case 47
  System.out.println("Enter the degree to get converted into radians");
  double userInput = input.nextDouble();
  
  // put the radiants into a variable
  double radians = (Math.toRadians(userInput));

  // translate input into radiants
  System.out.println(userInput + " degrees " + " is " + radians + " radians ");

  }
}

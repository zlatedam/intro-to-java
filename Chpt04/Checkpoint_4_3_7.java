import java.util.Random;

// Print out a random lowercase letter

public class Checkpoint_4_3_7 {
    public static void main(String [] args) {
        int randomNumber = (int)(Math.random() * 26 + 'a');

        // Print result
        System.out.println("Your random number is " + randomNumber);
    }
}
i

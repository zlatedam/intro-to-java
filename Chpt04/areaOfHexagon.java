import java.util.Scanner;
import java.lang.Math;

public class areaOfHexagon {
    public static void main(String [] args) {
	Scanner input = new Scanner(System.in);

	// Prompt user to enter the side
	System.out.println("Enter the side: ");
	double side = input.nextDouble();
	
	// Compute the area of the hexagon
	double area = (6 * Math.pow(side,2)) / (4 * (Math.tan(Math.PI / 6)));

	// Print the area
	System.out.printf("The area of the hexagon is %5.2f\n", area);

    }
}
									  

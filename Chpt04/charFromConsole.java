import java.util.Scanner;

// Create a main arguement 
public class charFromConsole {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);

        // Prompt user
        System.out.println("Enter a character");
        String s = input.nextLine();
        char ch = s.charAt(0);

        // print results
        System.out.println("The character entered is " + ch);
    }
}

public class Checkpoint_4_6_3 {
    public static void main(String [] args) {
        // Show the output of the following print statements
        System.out.printf("Amount is %f %e\n", 32.32, 32.32);
        System.out.printf("amount is %5.2f%% %5.4e\n", 32.327, 32.32);
        System.out.printf("%6b\n", (1 > 2));
        System.out.printf("%6s\n", "Java");
        System.out.printf("%6b%-8s\n", (1 > 2), "Java");
        System.out.printf(""%,5d %,6.1f\n", 312342, 315562.932);
        System.out.printf("%05d %06.1f\n", 32, 32.32);

    }
}

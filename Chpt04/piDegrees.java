import java.lang.Math;

public class piDegrees {
  public static void main(String [] args) {

  // translate pi to degrees and assign it to variable
  double piDegrees = Math.toDegrees(Math.PI);

  // print out results
  System.out.println("PI is = " + piDegrees + " degrees");

  }
}


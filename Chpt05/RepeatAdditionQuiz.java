import java.util.Scanner;

public class RepeatAdditionQuiz {
    public static void main(String [] args) {
	Scanner input = new Scanner(System.in);

	// 2 random numbers
	int number1 = (int)(Math.random() * 10);
	int number2 = (int)(Math.random() * 10);
	
	// Ask user to answer sum of number1 + number2
	System.out.println("What is " + number1 + " + " + number2 + " ? ");
	int answer = input.nextInt();

	// Loop to see if user got answer correct, continue asking until it is
	while (number1 + number2 != answer) {
	    System.out.println("Wrong answer. Try again. What is " + number1 + " + " + number2 + " ? ");
	    answer = input.nextInt();
	}
	System.out.println("You got it!");
    }
}


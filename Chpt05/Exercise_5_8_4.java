// What is wrong with the following program

// Program A
public class Exercise_5_8_4 {
   public static void main(String[] args) {
     int i = 0;
     do {
       System.out.println(i + 4);
       i++;
     }
     while (i < 10)
   }
 }


// Program B
public class Exercise_5_8_4 {
   public static void main(String[] args) {
     for (int i = 0; i < 10; i++);
        System.out.println(i + 4);
    }
  }

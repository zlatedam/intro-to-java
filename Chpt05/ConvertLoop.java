/* Convert the following for loop statement to a while loop and to a do-while loop

long sum = 0;
for (int i = 0; i <= 1000; i++)
sum = sum + i; */

public class ConvertLoop {
    public static void main (String [] args) {



	// While loop
	long sum = 0;
	int i =0;
	while (i <= 1000) {
	    sum += i++;
	}

	System.out.println("Sum is " +sum);
	System.out.println("i is " + i);

	// Do-while loop
	long sum = 0; // this line cant be executed because it has already been declared
	int i = 0;
	do {
	    sum += i++;
	}
	while (i <= 1000);
    }
}
